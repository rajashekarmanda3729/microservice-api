const express = require('express')
const app = express()
const dotenv = require('dotenv')
dotenv.config()
const PORT = process.env.PORT || 5000


app.use('/api/routes/users', require('./api/routes/users'))


app.use((err, req, res, next) => {
    res.json({ response: err })
})

app.listen(PORT, () => console.log(`Server running on PORT: ${PORT}`))




