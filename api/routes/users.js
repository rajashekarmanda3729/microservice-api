const { default: axios } = require('axios')
const express = require('express')
const router = express.Router()

const getData = async () => {
    const response = await axios('https://fakestoreapi.com/products')
    return response
}

router.get('/', async (req, res, next) => {
    const data = await getData()
    res.json(data.data)
})

router.post('/', (req, res) => {
    res.json({ response: 'response post' })
})

module.exports = router






